from tkinter import *
import string

def traduction_simple(texte, decalage) :
    trad = ""
    for lettre in texte :
        lettre = chr(ord(lettre) + decalage)
        trad += lettre
    return trad

def traduction_circulaire(texte, decalage) :
    trad = ""

    tab_min = string.ascii_lowercase[decalage:]+string.ascii_lowercase[:decalage]
    tab_maj = string.ascii_uppercase[decalage:]+string.ascii_uppercase[:decalage]

    for lettre in texte :
        if(lettre in string.ascii_lowercase):
            trad+=tab_min[ord(lettre)-ord('a')]
        else:
            if(lettre in string.ascii_uppercase):
                trad+=tab_maj[int(ord(lettre)-ord('A'))]
            else:
                trad += traduction(lettre, decalage)
    return trad


DECALAGE = 0


def click_crypt():

    DECALAGE = int(spinbox_decalage.get())
    if(rb_valeur_coche.get()=='1'):
        phrase_crypt.set(traduction_simple(phrase.get(), DECALAGE))
    if(rb_valeur_coche.get()=='2'):
        phrase_crypt.set(traduction_circulaire(phrase.get(), DECALAGE))


frame = Tk()
frame.title("Chiffrement de César")

phrase = StringVar()
phrase.set("")

phrase_crypt = StringVar()
phrase_crypt.set("")

label1=Label(frame,text="Saisir votre phrase")
label1.grid(column=0,row=0)

label2=Label(frame,text="Saisir votre decalage")
label2.grid(column=1,row=0)

label3=Label(frame,text="Votre phrase crypté s'affiche ici: ")
label3.grid(column=2,row=0)

input_phrase = Entry(frame, textvariable=phrase)
input_phrase.grid(column=0, row=1)

spinbox_decalage = Spinbox(frame, from_=1, to=25)
spinbox_decalage.grid(column=1, row=1)

output_phrase = Entry(frame, textvariable=phrase_crypt)
output_phrase.configure(state="disabled")
output_phrase.grid(column=2, row=1)

bouton_cryptage=Button(frame, text="Cryptage", command=click_crypt)
bouton_cryptage.grid(column=2,row=2)


rb_valeur_coche = StringVar()
rb_chiffrement = Radiobutton(frame, text="chiffrement simple", variable=rb_valeur_coche, value=1)
rb_chiffrement_circulaire = Radiobutton(frame, text="chiffrement circulaire", variable=rb_valeur_coche, value=2)
rb_chiffrement.grid(column=0,row=2)
rb_chiffrement_circulaire.grid(column=1,row=2)
rb_chiffrement.select()

frame.mainloop()